public class Date {
 /**
  * Thuộc tính
  */
 private int day;
 private int month;
 private int year;
 /**
  * Phương thức
  */
 public Date (int day , int month , int year){
  if (day <= 31 && day > 0) {
   this.day = day;
  }
   if (month <= 12 && month >= 1 ) {
   this.month = month;
  } 
  if(year <= 9999 && year >= 1900) {
   this.year = year;
  }
 }
 /**
  * Getter method
  * @return
  */
 public int getDay() {
  return this.day;
 }
 public int getMonth() {
  return this.month;
 }
 public int getYear() {
  return this.year;
 }
 /**
  * Setter method
  * @param
  */
  public void setDay(int day) {
   if (day <= 31 && day > 0) {
    this.day = day;
   }
  }
  public void setMonth(int month) {
   if (month <= 12 && month > 0) {
    this.month = month;
   } 
  }
  public void setYear(int year) {
   if(year <= 9999 && year >= 1900) {
    this.year = year;
   }
  }
  public void setDate(int day, int month, int year) {
   this.day = day;
   this.month = month;
   this.year = year;
  }
  public String toString(){
   return day+ "/"+ month+ "/"+ year;
  }
}
